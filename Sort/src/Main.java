import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
       /* int [] mass = new int[] {2, 0, 24, 3477, 8,10};
        boolean isNeed = false;
        while (!isNeed) {
            isNeed = true;
            for (int i = 0; i < mass.length - 1; i++) {
                if (mass[i] > mass[i + 1]) {
                    int t = mass[i];
                    mass[i] = mass[i + 1];
                    mass[i + 1] = t;
                    isNeed = false;
                }
            }
        }

        for (int i=0;i<mass.length;i++)
        {
            System.out.println(mass[i]);
        }*/

        int[] array = {10, 3, 53, 2, 5, 7, 3, 6, 12, 3, 32, 65}; //исходный массив
        // Сортировка пузырьком
        int temp;                                               // переменная для замены
        for (int i = array.length - 1; i > 0; i--) {            // внешний цикл
            for (int j = 0; j < i; j++) {                       // внутренний цикл
                if (array[j] > array[j + 1]) {                  // сравнение
                    temp = array[j];                            // если истино, то меняем
                    array[j] = array[j + 1];                    // местами элементы
                    array[j + 1] = temp;
                }
            }
        }
        // конец сортировки
        System.out.println(Arrays.toString(array));
    }

}
