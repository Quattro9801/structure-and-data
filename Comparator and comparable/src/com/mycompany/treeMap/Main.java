package com.mycompany.treeMap;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();// хранит элементы в отсортированном виде
        treeMap.put(986,"Arseny");
        treeMap.put(1,"Evgen");
        treeMap.put(10000,"Nataly");
        treeMap.put(678,"Vlad");
        System.out.println(treeMap);
        System.out.println(treeMap.descendingMap());// вывод в обратном порядке
        System.out.println(treeMap.tailMap(986));// вывести начиная с
        System.out.println(treeMap.headMap(986)); // с хвоста
        System.out.println(treeMap.lastEntry());//последний элемент
        System.out.println(treeMap.firstEntry());//первый элемент
        // В качестве ключа объект
        Person p1 = new Person("Ivan", "Nokolaev");
        Person p2 = new Person("Olga", "Buzova");
        Person p3 = new Person("Dmitriy", "Oleinik");
        Person p4 = new Person("Vova", "Smirnov");
        Person p5 = new Person("Vova2345", "Smirnov");
        TreeMap<Person, Double> treeMap1 = new TreeMap<>();
        treeMap1.put(p1, 4.2);
        treeMap1.put(p2,3.8);
        treeMap1.put(p3,5.0);
        treeMap1.put(p4,3.4);
        treeMap1.put(p5,null);
        System.out.println(treeMap1);
    }
}
