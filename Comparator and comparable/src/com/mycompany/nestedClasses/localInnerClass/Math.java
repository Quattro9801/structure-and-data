package com.mycompany.nestedClasses.localInnerClass;

public class Math {
    public void getResult() {
        class Delenie {
            private int delimoe;
            private int delitel;

            public int getDelimoe() {
                return delimoe;
            }

            public void setDelimoe(int delimoe) {
                this.delimoe = delimoe;
            }

            public int getDelitel() {
                return delitel;
            }

            public void setDelitel(int delitel) {
                this.delitel = delitel;
            }
            public int getRes() {
                return delimoe/delitel;
            }
            public int getMod() {
                return delimoe%delitel;
            }
        }

        Delenie delenie = new Delenie();
        delenie.setDelimoe(10);
        delenie.setDelitel(5);
        System.out.println(delenie.getRes());
        System.out.println(delenie.getMod());
    }
}
