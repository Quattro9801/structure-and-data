package com.mycompany.nestedClasses.innerClasses;

import com.mycompany.nestedClasses.staticClass.Car;

public class MAin {
    public static void main(String[] args) {
      /*  CarForInner car = new CarForInner("Black", 4,100);
        System.out.println(car);
*/
        CarForInner carForInner = new CarForInner("Black", 4);
        CarForInner.Engine engine = carForInner.new Engine(40);// создание внутреннего объекта
        carForInner.setEngine(engine);
        System.out.println(carForInner);
        //второй способ создания внутреннего класса
        CarForInner.Engine engine2 = new CarForInner("Blue",3).new Engine(30);
        System.out.println(engine2);

    }
}
