package com.mycompany.nestedClasses.innerClasses;

import com.mycompany.nestedClasses.staticClass.Car;

public class CarForInner {
    String color;
    int doorCount;
    Engine engine;
    private static int extStatField;

    public CarForInner(String color, int doorCount) {
        this.color = color;
        this.doorCount = doorCount;
        //*this.engine = this.new Engine(100); один из вариантов создания
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    void method() {
    }
    public  class Engine {
        private int horsePower;


        public Engine(int horsePower) {
            System.out.println(extStatField);//могу обратиться к статической переменной внешнего класса
            //System.out.println(doorCount);не могу обратиться к переменным внешнего класса
            this.horsePower = horsePower;
        }

        @Override
        public String toString() {
            return "Engine{" +
                    "horsePower=" + horsePower +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", doorCount=" + doorCount +
                ", engine=" + engine +
                '}';
    }
}

