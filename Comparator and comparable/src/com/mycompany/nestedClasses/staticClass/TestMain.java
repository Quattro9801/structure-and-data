package com.mycompany.nestedClasses.staticClass;

public class TestMain {
    public static void main(String[] args) {
        Car.Engine engine = new Car.Engine(100);
        System.out.println(engine);
        Car car = new Car("blue",4,engine);
        System.out.println(car);

    }
}
