package com.mycompany.nestedClasses.staticClass;

public class Car {
    String color;
    int doorCount;
    Engine engine;
    private static int extStatField;

    public Car(String color, int doorCount, Engine engine) {
        this.color = color;
        this.doorCount = doorCount;
        this.engine = engine;
    }
    void method() {
        System.out.println(Engine.engineCount);//обращение к приватной переменной внутреннего статического класса
        Engine engine1 = new Engine(100);
        System.out.println(engine1.horsePower);//обращение к приватной нестатической переменной
    }
    public static class Engine {
        private int horsePower;
       private static int engineCount;

        public Engine(int horsePower) {
            System.out.println(extStatField);//могу обратиться к статической переменной внешнего класса
            //System.out.println(doorCount);не могу обратиться к переменным внешнего класса
            this.horsePower = horsePower;
        }

        @Override
        public String toString() {
            return "Engine{" +
                    "horsePower=" + horsePower +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", doorCount=" + doorCount +
                ", engine=" + engine +
                '}';
    }
}
