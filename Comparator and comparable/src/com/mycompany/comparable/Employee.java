package com.mycompany.comparable;

import java.util.List;
import java.util.Objects;

public class Employee implements Comparable<Employee> {

    private int id;
    private String name;
    private String surname;
    private String salary;
    private Location location;

    public Employee(int id, String name, String surname, String salary, Location location) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.salary = salary;
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSalary() {
        return salary;
    }

    @Override
    public int compareTo(Employee anotherEmployee) {
        return this.id - anotherEmployee.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", salary='" + salary + '\'' +
            ", location=" + location +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id && Objects.equals(name, employee.name) && Objects.equals(surname, employee.surname) && Objects.equals(salary, employee.salary);
    }


}
