package com.mycompany.comparable;

import java.util.Comparator;

public class EmployeeComparator implements Comparator<Employee> {
    @Override
    public int compare(Employee emp1, Employee em2)
    {
        return emp1.getLocation().getLocname().compareTo(em2.getLocation().getLocname());
    }
}
