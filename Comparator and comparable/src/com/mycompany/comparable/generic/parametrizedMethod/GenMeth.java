package com.mycompany.comparable.generic.parametrizedMethod;

import java.util.ArrayList;
import java.util.List;

public class GenMeth {

    public static  <T> T getSecondElem(List<T> arrayList) {//если мы пишем тип<T> только у метода, то он может быть другим не как у класса
        return arrayList.get(1);
    }
}
