package com.mycompany.comparable.lambda;

import java.util.ArrayList;
import java.util.List;

public class ConsumerTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Privet");
        list.add("Poka");
        list.add("Hi");
        list.add("Again");
        list.forEach(s-> System.out.println(s.length()));
    }
}
