package com.mycompany.comparable.lambda;

import java.util.*;
import java.util.function.Predicate;

public class StudentInfo {
    public static void main(String[] args) {
        Student student = new Student("Ivan",'M',23, 5);
        Student student2 = new Student("Dmitriy",'M',24, 1);
        Student student3= new Student("Elisa",'F',23, 2);
        Student student4 = new Student("Egor",'F',20, 2);
        Student student5 = new Student("Ilya",'M',20, 1);
        List<Student> list = new ArrayList<>();
        list.add(student);
        list.add(student2);
        list.add(student3);
        list.add(student4);
        list.add(student5);

        StudentInfo studentInfo = new StudentInfo();
        studentInfo.printStudentUnderCourse(list, 2);
        studentInfo.printStudentsUnderAge(list, 23);
        System.out.println("---------------------------------------------------------------");
        studentInfo.printstudentsUnderAge1(list, new CheckStudentsImp());

        System.out.println("---------------------------------------------------------------");
        System.out.println("With anonymous class");
        System.out.println("                                    ");
        studentInfo.printstudentsUnderAge1(list, new CheckStudents() {
            @Override
            public boolean check(Student student) {
                return student.getAge()<24;
            }
        });

        System.out.println("---------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------");
        System.out.println("With lambda expression");
        System.out.println("                                    ");
        System.out.println("  studentInfo.printstudentsUnderAge1(list, (Student s) ->\n" +
                "                                                        { return s.getCourse() == 5;\n" +
                "\n" +
                "        });");
        studentInfo.printstudentsUnderAge1(list, (Student s) ->
                                                        { return s.getCourse() == 5;// первый вариант написания, длинный

        });

        System.out.println("---------------------------------------------------------------");
        System.out.println("---------------------------------------------------------------123");
        studentInfo.printstudentsUnderAge1(list, (Student s ) ->
                                                            { return s.getName().equalsIgnoreCase("Ivan");});

        System.out.println("---------------------------------------------------------------");
        System.out.println("Different variant to write lambda");
        System.out.println("///////////////////////////////////");
        System.out.println("                                    ");
        System.out.println("Short variant");// Возможен если в качестве параметра 1 аргумент и одно условие в теле метода, если нет то пишем обязательно полную форму
        System.out.println("  studentInfo.printstudentsUnderAge1(list, s->\n" +
                "         s.getCourse() == 5\n" +
                "        );");
        // s в данной случае параметр метода (->) разделитель,  s.getCourse() == 5 - тело метода (по сути тело метода могло бы быть метододом соответствующего класса, который релизует метод интерфейса
        studentInfo.printstudentsUnderAge1(list, s->
         s.getCourse() == 5 // короткий вариант
        );

        System.out.println("///////////////////////////////////");
        System.out.println("Multivariant");
        System.out.println("                                    ");
        studentInfo.printstudentsUnderAge1(list, s->
        {
            return s.getCourse() == 5;
        });


        System.out.println("///////////////////////////////////");
        System.out.println("///////////////////////////////////");
        System.out.println("///////////////////////////////////");
        System.out.println("///////////////////////////////////");
        System.out.println("For two arguments");
        System.out.println("                                    ");
        //для 2 аргуметов
        studentInfo.printstudentsUnderAge2(list, (Student st, int age) -> {return st.getAge() > age && st.getAge() <  24 ;

        });
        System.out.println("                                    ");
        System.out.println("For zero arguments");
        System.out.println("                                    ");
        studentInfo.printName(list, () -> System.out.println("Hello"));
        System.out.println("                                    ");
        System.out.println("Можно вынести лямбда выражение в отдельную переменную");
        System.out.println("                                    ");

        CheckAgeStudent ageStudent = (st, age) ->
                                                {return st.getAge() > age && st.getAge() ==20;// переменные st и age видны только внутри лямбды
        };
        studentInfo.printstudentsUnderAge2(list, ageStudent);

        System.out.println("                                    ");
        System.out.println("Сортировка с помощью лямбды");
        System.out.println("                                    ");
        Collections.sort(list, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.getCourse() - o2.getCourse();
            }
        });
        System.out.println(list);
        Collections.sort(list, (st1, st2) -> {
            return st1.getCourse() - st2.getCourse();
        });
        System.out.println(list);
    }

    void printStudentUnderCourse(List<Student> students, int course){
        students
                .stream().filter(s->s.getCourse() < course )
                .forEach(s->System.out.println(s));

    }

    void printStudentsUnderAge(List<Student> list, int age) {
        list
                .stream().filter(s->s.getAge() > age)
                .forEach(s->System.out.println(s));

    }

    void printstudentsUnderAge1(List<Student> list, CheckStudents checkStudents){

        for (Student st:list){
            if (checkStudents.check(st)){
                System.out.printf("Студент " + st);
            }
        }

    }

    void printStudentsUnderAgePrdicate(List<Student> list, Predicate<Student> checkStudents){

        for (Student st:list){
            if (checkStudents.test(st)){
                System.out.printf("Студент " + st);
            }
        }

    }

    void printStudentsWithCourse(List<Student> list, Predicate<Student> checkStudents){

        for (Student st:list){
            if (checkStudents.test(st)){
                System.out.printf("Студент " + st);
            }
        }

    }

    void printName(List<Student> list, IName name){

        for (Student st:list){
            name.printName();
            System.out.println(st.getName());
        }
    }

    void printstudentsUnderAge2(List<Student> list, CheckAgeStudent ageStudent) { // с двумя аргументами для лямбды

        for (Student st:list){
            if (ageStudent.checkAge(st, 18)){
                System.out.printf("Студент " + st);
            }
        }

    }


}
// Функциональный интерфейс. Имеет только один абстрактный метод

interface CheckStudents {
    boolean check(Student student);
}
//принимаем 2 параметра
interface CheckAgeStudent {
    boolean checkAge(Student student, int age);
}

interface IName {
    void printName();
}

class CheckStudentsImp implements CheckStudents{

    @Override
    public boolean check(Student student) {
        return student.getAge()>23;
    }
}
