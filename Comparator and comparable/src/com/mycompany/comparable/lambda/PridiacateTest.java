package com.mycompany.comparable.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
//предикат имеет уже встроенный boolean метод, что позволяет не писать свой функциональный интерфейс
public class PridiacateTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Privet");
        list.add("Poka");
        list.add("Hi");
        list.add("Again");
        list.removeIf(s-> s.length() == 2);
        System.out.println(list);
        System.out.println("                                             ");
        System.out.println("Использование предиката для студентов");

        Student student = new Student("Ivan",'M',19, 5);
        Student student2 = new Student("Dmitriy",'M',19, 4);
        Student student3= new Student("Elisa",'F',23, 2);
        Student student4 = new Student("Egor",'F',20, 2);
        Student student5 = new Student("Ilya",'M',20, 1);
        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);
        studentList.add(student5);

        StudentInfo studentInfo = new StudentInfo();
        Predicate<Student> predicate1 = p -> p.getAge() > 20;
        Predicate<Student> predicate2 = p -> p.getCourse() >= 4;
        studentInfo.printStudentsUnderAgePrdicate(studentList, predicate1);
        System.out.println(" " +
                "                                            ");
        System.out.println("Можно комбинировать условия");
        studentInfo.printStudentsWithCourse(studentList, predicate1.and(predicate2));
        System.out.println(" OR");
        studentInfo.printStudentsWithCourse(studentList, predicate1.or(predicate2));
        System.out.println("Negate отрицание");
        studentInfo.printStudentsWithCourse(studentList, predicate1.negate());//?

    }

}
