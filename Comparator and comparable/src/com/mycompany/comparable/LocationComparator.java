package com.mycompany.comparable;

import java.util.Comparator;

public class LocationComparator implements Comparator<Location> {

    @Override
    public int compare(Location o1, Location o2) {
        return o1.getLocname().compareTo(o2.getLocname());
    }
}
