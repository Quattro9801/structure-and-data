package com.mycompany.comparable.stream;

import com.mycompany.comparable.Employee;
import com.mycompany.comparable.Location;

public class EmployeeStream {
    private int id;
    private String name;
    private String surname;
    private String salary;
    private Location location;

    public EmployeeStream(int id, String name, String surname, String salary, Location location) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.salary = salary;
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSalary() {
        return salary;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", salary='" + salary + '\'' +
                ", location=" + location +
                '}';
    }
}
