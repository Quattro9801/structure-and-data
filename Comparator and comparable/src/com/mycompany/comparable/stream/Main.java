package com.mycompany.comparable.stream;

import com.mycompany.comparable.Location;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        /////Метод MAP
        List<String> list = new ArrayList<>();
        list.add("privet");
        list.add("hi");
        list.add("bye");
        list.add("la");

        List<Integer> newList = list.stream().map(elem -> elem.length())
                .collect(Collectors.toList());
        System.out.println(newList);
        int[] arr = {5, 9, 3, 8, 1};
        arr = Arrays.stream(arr).map(elem -> {
            if (elem % 3 == 0) {
                elem = elem / 3;
            }
            return elem;
        }).toArray();
        System.out.println(Arrays.toString(arr));

        //filter работает как Predicate
        List<EmployeeStream> employeeStreams = new ArrayList<>();
        EmployeeStream employeeStream = new EmployeeStream(1, "Ivan", "Ivanov", "600", new Location(1, "NN"));
        EmployeeStream employeeStream2 = new EmployeeStream(2, "Nik", "Ivanov", "600", new Location(1, "NN"));
        EmployeeStream employeeStream3 = new EmployeeStream(3, "Vladimir", "ALekseev", "900", new Location(1, "st"));
        EmployeeStream employeeStream4 = new EmployeeStream(4, "Solovey", "Ivanov", "100", new Location(1, "Moscow"));
        EmployeeStream employeeStream5 = new EmployeeStream(5, "Egor", "Ivanov", "50", new Location(1, "NN"));
        employeeStreams.add(employeeStream);
        employeeStreams.add(employeeStream2);
        employeeStreams.add(employeeStream3);
        employeeStreams.add(employeeStream4);
        employeeStreams.add(employeeStream5);
        List<EmployeeStream> filtereList = employeeStreams.stream().filter(emp -> {
                    System.out.println("12345");

                return Integer.parseInt(emp.getSalary()) >= 50 && Integer.parseInt(emp.getSalary()) <= 100;}).collect(Collectors.toList());
        System.out.println(filtereList);
        /// foreach
        System.out.println("ForEach");
        employeeStreams.stream().forEach(el -> {
            int i = Integer.parseInt(el.getSalary()) * 2;
            System.out.println(i);
        });


        //sorted
        List<EmployeeStream> employee = employeeStreams.stream().sorted((x,y)-> x.getSurname().compareTo(y.getSurname())).collect(Collectors.toList());
        System.out.println(employee);

        List<Integer> reduceList = new ArrayList<>();
        reduceList.add(1);
        reduceList.add(2);
        reduceList.add(3);
        reduceList.add(4);
        reduceList.add(5);
        reduceList.add(6);
        reduceList.add(7);
        reduceList.add(8);
        int result = reduceList.stream().reduce((accumulator,element) -> accumulator + element).get();//accum- суммирующий элемент,который сохраняет предыдущие значения
        System.out.println(result);
        int resul2 = reduceList.stream().reduce(1,(accumulator,element) -> accumulator + element);//identity - начальное значение аккумулятора
        System.out.println(resul2);
       int allSalary =  employeeStreams.stream().
               map(el->Integer.parseInt(el.getSalary())).
               reduce((sum,el)-> sum + el).get();
        System.out.println("Salary " + allSalary);
        System.out.println("---------------------------------------------");
        System.out.println("Concat");
        Stream<Integer> st1 =  Stream.of(1,2,3,4,5,6);
        Stream<Integer> st2 =  Stream.of(99,4,53,4);
        Stream.concat(st1,st2).forEach(System.out::println);
        System.out.println("---------------------------------------------");
        System.out.println("Distinct");
        Stream<Integer> st3 =  Stream.of(99,4,53,4,4);
        st3.distinct().forEach(System.out::println);
        System.out.println("---------------------------------------------");
        System.out.println("Count");
        Stream<Integer> st4 =  Stream.of(99,4,53,4,4);
        System.out.println(st4.distinct().peek(System.out::println).count());//peek выводит промежуточные результаты аналог foreach
        System.out.println("---------------------------------------------");
        System.out.println("groupingBy");

        Map<String, List<EmployeeStream>> st =  employeeStreams.stream().collect(Collectors.groupingBy(el-> el.getLocation().getLocname()));
        for (Map.Entry<String, List<EmployeeStream>> s:st.entrySet()) {
            System.out.println(s);
        }

        System.out.println("---------------------------------------------");
        System.out.println("partitionBy");

        Map<Boolean, List<EmployeeStream>> partBy =  employeeStreams.stream().collect(Collectors.partitioningBy(el-> Integer.parseInt(el.getSalary()) < 100));
        for (Map.Entry<Boolean, List<EmployeeStream>> s:partBy.entrySet()) {
            System.out.println(s);
        }

    }
}
