package com.mycompany.comparable;

public class Location {
    private int id;
    private String locname;

    public Location(int id, String locname) {
        this.id = id;
        this.locname = locname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocname() {
        return locname;
    }

    public void setLocname(String locname) {
        this.locname = locname;
    }

    @Override
    public String toString() {
        return "Location{" +
            "id=" + id +
            ", locname='" + locname + '\'' +
            '}';
    }
}
