package com.mycompany.concurrency.atomicInteger;

public class MyThread implements Runnable {
    @Override
    public void run() {
        for (int i=0;i<100;i++) {
            AtomicClass.increment();
        }
        System.out.println();
    }
}
