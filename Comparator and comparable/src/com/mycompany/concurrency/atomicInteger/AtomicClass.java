package com.mycompany.concurrency.atomicInteger;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicClass {
    public static AtomicInteger counter = new AtomicInteger(0);

    public static void increment() {
        //counter.incrementAndGet();
        counter.addAndGet(5);//увеличивает на требуемое кол-во
    }
}
