package com.mycompany.concurrency;

public class MessangerMeth
{
    private static final Object lock = new Object();

    public void callSkype() {
        synchronized (lock) {
            System.out.println("Start skype call");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("End skype call");
        }
    }
    public void callViber() {
        synchronized (lock) {
            System.out.println("Start Viber call");
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("End Viber call");
        }
    }

    public void callTelegram() {
        synchronized (lock) {
            System.out.println("Start Telegram call");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("End Telegram call");
        }
    }
}
