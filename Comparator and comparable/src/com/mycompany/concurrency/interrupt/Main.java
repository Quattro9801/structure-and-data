package com.mycompany.concurrency.interrupt;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main start");
        InterruptedThread thread = new InterruptedThread();
        thread.start();
        Thread.sleep(2000);
        thread.interrupt();//говорит только о том, что поток хотят прервать, но поток не будет прерван
        thread.join();
        System.out.println("Main end");

    }
}
