package com.mycompany.concurrency.interrupt;

public class InterruptedThread extends Thread {
    @Override
    public void run() {
        double sqrtSum = 0;
        for (int i = 0;i<1000000000;i++) {
            if (isInterrupted()) {
                System.out.println("Thread was interrupted");
                return;
            }
             sqrtSum += Math.sqrt(i);
            try {
                sleep(10000);
            } catch (InterruptedException e) {
                System.out.println("Someone wants to interrupt thread");
                return;
            }
        }
        System.out.println("Sum= "+ sqrtSum);
    }
}
