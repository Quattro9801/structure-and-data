package com.mycompany.concurrency.wait.notify;


public class Logic {
    private static final Object monitor = new Object();
    private static boolean ready = false;

    public void prepareData() {
        synchronized (monitor) {//2 запускаем данный поток
            ready = true;
            System.out.println("Data was prepared");
            monitor.notify();//3 кидаем нотификацию, чтобы второй поток продолжил работу
        }
    }

    public void download() {
        System.out.println("Starting download");
    }

    public void loadToStore() {
        synchronized (monitor) {
            System.out.println("Start loading data");
            if (!ready) {
                try {
                    monitor.wait();// 1.открываем монитор
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("Success");// продолжаем работу
        }
    }
}
