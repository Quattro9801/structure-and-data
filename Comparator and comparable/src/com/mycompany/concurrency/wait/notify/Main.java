package com.mycompany.concurrency.wait.notify;

public class Main
{
    public static void main(String[] args) throws InterruptedException {
        Producer producer = new Producer();
        Thread threadProd = new Thread(producer);
        Consumer consumer = new Consumer();
        Thread threadCons = new Thread(consumer);
        threadCons.start();
        threadProd.start();
        threadCons.join();
        threadProd.join();
        System.out.println("End");
    }
}
