package com.mycompany.concurrency.wait.notify;

public class Consumer implements Runnable {
    Logic logic = new Logic();
    @Override
    public void run() {
            logic.prepareData();
    }
}
