package com.mycompany.concurrency;

public class VolatileEx extends Thread {
    volatile boolean flag = true;

    public void run() {
        long counter = 0;
        while (flag) {
            counter ++;
        }
        System.out.printf("Loop is finished. Counter = " + counter);

    }

    public static void main(String[] args) throws InterruptedException {
        VolatileEx volatileEx =  new VolatileEx();
        volatileEx.start();
        Thread.sleep(3000);// на маин потоке
        System.out.println("After 3 seconds it is time to wake up!");
        volatileEx.flag = false;
        volatileEx.join();// ждем завершения потока
        System.out.println("End");

    }
}
