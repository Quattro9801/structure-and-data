package com.mycompany.concurrency.daemon;

import java.util.concurrent.locks.Lock;

public class DaemonThread extends Thread{
    @Override
    public void run() {
        Logic logic = new Logic();
        logic.getInt();
    }
}
