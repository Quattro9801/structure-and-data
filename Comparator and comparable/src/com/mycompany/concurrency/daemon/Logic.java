package com.mycompany.concurrency.daemon;

public class Logic {

    public void getSymbol() {
        System.out.println("Is Daemon " + Thread.currentThread().isDaemon());
        for (char s ='A';s<='J';s++) {
            System.out.println(s);
        }
    }

    public void getInt() {
        System.out.println("Is Daemon " + Thread.currentThread().isDaemon());
        for (int s =1;s<1000;s++) {
            System.out.println(s);
        }
    }
}
