package com.mycompany.concurrency.daemon;

public class DaemonMain {
    public static void main(String[] args) {
        UsualThread usualThread = new UsualThread();
        DaemonThread daemonThread = new DaemonThread();
        daemonThread.setDaemon(true);// присвоение идет до старта потока
        usualThread.start();
        daemonThread.start();
    }
}
