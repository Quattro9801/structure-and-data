package com.mycompany.concurrency;

public class Ex1 {
    public static void main(String[] args) {
        //создание потока через анонимный класс
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Privet");
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Hi");
            }
        }).start();
    }
}
