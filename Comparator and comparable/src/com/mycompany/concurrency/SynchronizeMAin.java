package com.mycompany.concurrency;

public class SynchronizeMAin {
    public static void main(String[] args) {
        SynchronizeThread1 synchronizeThread1 = new SynchronizeThread1();
        Thread thread1 = new Thread(synchronizeThread1);
        Thread thread2 = new Thread(synchronizeThread1);
        Thread thread3 = new Thread(synchronizeThread1);
        thread1.start();
        thread2.start();
        thread3.start();


    }
}
