package com.mycompany.concurrency;
//методы sleep и join в потоках
public class SleepJoinMain {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new MyRunnable());
        System.out.println(thread.getState());
        MyRunnable2 myRunnable2 = new MyRunnable2();
        System.out.println(myRunnable2.getState());
        thread.start();
        System.out.println(thread.getState());
        myRunnable2.start();
        System.out.println(myRunnable2.getState());
        thread.join();
        System.out.println(thread.getState());
        myRunnable2.join();
        System.out.println(myRunnable2.getState());
        //myRunnable2.join(1000);//параметр указывает, что основной поток либо ждет завершения этого потока либо 1 секунду, если поток выполняется дольше, соответственно сообщение "End" появится через 1 секунду
        System.out.println("End");//выводится первым так как маин поток запустил 2 потока и не ждет их а идет дальше и выводит сообщение, чтобы поток ждал выполнения других используется join


    }
}
