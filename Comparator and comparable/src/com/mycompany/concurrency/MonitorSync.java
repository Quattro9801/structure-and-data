package com.mycompany.concurrency;
//например задача// есть 3 потока какждый из которых принимает входящий звонок по своему приложению (Skype,Viber,Telegram).Пока не закончился звонок по одной линии мы не можем взять трубку
public class MonitorSync {
    public static void main(String[] args) throws InterruptedException {
        Thread skype = new Thread(new SkypeThread());
        Thread viber = new Thread(new ViberThread());
        Thread telegram= new Thread(new TelegramThread());
        skype.start();
        viber.start();
        telegram.start();
        skype.join();
        viber.join();
        telegram.join();
        System.out.println("-----------------------------------------------");
        System.out.println("End calling");


    }
}
