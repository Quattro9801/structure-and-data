package com.mycompany.concurrency.countDownLatch;

import java.util.concurrent.CountDownLatch;

public class Main {
    static CountDownLatch countDownLatch = new CountDownLatch(3);// передаем кол-во задач, когда 3 задачи будут выполнены то другие потоки смогут начать работу
    public static void getStuffOnThePlace() throws InterruptedException {
        Thread.sleep(3000);
        System.out.println("Охрана пришла");
        countDownLatch.countDown();// уменьшаем счетчик
        System.out.println("Счетчик задач = " + countDownLatch.getCount());
    }
    public static void turnOnLight() throws InterruptedException {
        Thread.sleep(4000);
        System.out.println("Включился свет");
        countDownLatch.countDown();// уменьшаем счетчик
        System.out.println("Счетчик задач = " + countDownLatch.getCount());
    }
    public static void openDoors() throws InterruptedException {
        Thread.sleep(4000);
        System.out.println("Двери открыты");
        countDownLatch.countDown();// уменьшаем счетчик
        System.out.println("Счетчик задач = " + countDownLatch.getCount());
    }
    public static void main(String[] args) throws InterruptedException {
        Friend friend = new Friend("Ivan", countDownLatch);
        Friend friend2 = new Friend("Dmitrii", countDownLatch);
        Friend friend3 = new Friend("Elena", countDownLatch);
        Friend friend4 = new Friend("Vasya", countDownLatch);
        getStuffOnThePlace();
        turnOnLight();
        openDoors();

    }
}
