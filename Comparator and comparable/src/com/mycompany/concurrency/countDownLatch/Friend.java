package com.mycompany.concurrency.countDownLatch;

import java.util.concurrent.CountDownLatch;

public class Friend extends Thread {
    String name;
    CountDownLatch countDownLatch;

    public Friend(String name, CountDownLatch countDownLatch) {
        this.name = name;
        this.countDownLatch = countDownLatch;
        start();
    }
    public void run() {
        //System.out.println("Готов идти на распродажу " + name);
        try {
            countDownLatch.await();//ждем пока счетчик станет равен нулю
            System.out.println("Закупается " + name);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
