package com.mycompany.concurrency;

public class NameThreadMain {
    public static void main(String[] args) {
        MyThread5 myThread5 = new MyThread5();
        myThread5.setName("MyThread123");
        System.out.println(myThread5.getName());
        System.out.println(myThread5.getPriority());
        MyThread5 myThread6 = new MyThread5();
     /*   myThread6.setPriority(Thread.MIN_PRIORITY); приоритеты потоков
        myThread6.setPriority(Thread.MAX_PRIORITY);
        myThread6.setPriority(Thread.NORM_PRIORITY);*/

        myThread6.setName("Mythread123456");
        System.out.println(myThread6.getName());
        System.out.println(myThread6.getPriority());

    }

}
