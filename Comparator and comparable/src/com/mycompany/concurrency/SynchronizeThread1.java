package com.mycompany.concurrency;

public class SynchronizeThread1 implements Runnable {
   synchronized public void increment(){
       System.out.println(this.toString());// слово synchronize как бы ставит замок и говорит, что пока один поток работает с переменной, другие потоки не могут ее менять, они должны ждать завершения потока
        Counter.count++;
        System.out.println(Counter.count + " ");
    }

    @Override
    public void run() {
        for (int i=0;i<3;i++) {

            increment();
        }
    }
}
