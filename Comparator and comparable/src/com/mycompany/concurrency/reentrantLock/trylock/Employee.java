package com.mycompany.concurrency.reentrantLock.trylock;

import java.util.concurrent.locks.Lock;

public class Employee implements Runnable{
    String name;
    Lock lock;
    @Override
    public void run() {
        if (lock.tryLock()) {//  проверяет, заблокирован ли уже поток, если нет то блокирует
            //lock.lock();
            try {
                System.out.println(name + " начал пользоваться банкоматом");
                Thread.sleep(2000);
                System.out.println(name + " закончил пользоваться банкоматом");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                lock.unlock();
            }
        }
        else System.out.println("Не хочет ждать очереди " + name);
    }

    public Employee(String name, Lock lock) {
        this.name = name;
        this.lock = lock;
    }
}
