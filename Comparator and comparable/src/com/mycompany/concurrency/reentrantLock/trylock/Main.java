package com.mycompany.concurrency.reentrantLock.trylock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Lock lock = new ReentrantLock();
        Thread thread1 = new Thread(new Employee("Ivan", lock));
        Thread thread2 = new Thread(new Employee("Elena", lock));
        Thread thread3 = new Thread(new Employee("Vlad", lock));
        Thread thread4 = new Thread(new Employee("Andrei", lock));
        Thread thread5 = new Thread(new Employee("Anna", lock));
        Thread thread6 = new Thread(new Employee("Alex", lock));
        thread1.start();
        thread2.start();
        thread4.start();
        thread3.start();
        Thread.sleep(5000);
        thread5.start();
        thread6.start();

    }

}
