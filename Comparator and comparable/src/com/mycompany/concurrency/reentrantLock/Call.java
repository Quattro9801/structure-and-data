package com.mycompany.concurrency.reentrantLock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Call {
   private final Lock lock1 = new ReentrantLock();
    void telegramCall() {
        lock1.lock();
        try {
            System.out.println("Start telegram call");
            Thread.sleep(6000);
            System.out.println("End telegram call");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        finally {
            lock1.unlock();
        }
    }

    void viberCall() {
        lock1.lock();
        try {
            System.out.println("Start viber call");
            Thread.sleep(5000);
            System.out.println("End viber call");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        finally {
            lock1.unlock();
        }
    }

    void whatsappCall() {
        lock1.lock();
        try {
            System.out.println("Start whatsapp call");
            Thread.sleep(6000);
            System.out.println("End whatsapp call");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        finally {
            lock1.unlock();
        }
    }
}
