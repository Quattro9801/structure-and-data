package com.mycompany.concurrency;

public class Ex2 extends Thread {
    public static void main(String[] args) {
        //1 вариант создания потока
        MyThread1 myThread1 = new MyThread1();
        MyThread2 myThread2 = new MyThread2();
        myThread1.start();// в таком случае потоки работаю рандомно, потки не синхронизированы
        myThread2.start();
    }

}
