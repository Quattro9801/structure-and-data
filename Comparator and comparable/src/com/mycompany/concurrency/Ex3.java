package com.mycompany.concurrency;

public class Ex3 {
    public static void main(String[] args) {
        //2 вариант создания потока
        Thread thread3 = new Thread(new MyThread3());
        Thread thread4 = new Thread(new MyThread4());
        thread3.start();
        thread4.start();
    }
}
