package com.mycompany.concurrency.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i=0;i<10;i++) {
            executorService.execute(new ThreadEx());
        }
        executorService.shutdown();// завершает выполнение, говорит о том что задач больше не будет и можно, останавливать выполнение
        executorService.awaitTermination(5, TimeUnit.SECONDS);
        System.out.println("End");
    }
}
