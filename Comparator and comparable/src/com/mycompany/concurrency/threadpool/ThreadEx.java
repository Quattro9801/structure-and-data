package com.mycompany.concurrency.threadpool;

public class ThreadEx extends Thread {
    @Override
    public void run() {
        System.out.println("Start");
        System.out.println(Thread.currentThread().getName());

        try {
            sleep(6000);
            System.out.println("Finish");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
