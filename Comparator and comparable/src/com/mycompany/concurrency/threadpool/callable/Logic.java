package com.mycompany.concurrency.threadpool.callable;

import java.util.concurrent.Callable;

public class Logic implements Callable<Integer> {
    int a;

    public Logic(int a) {
        this.a = a;
    }

    @Override
    public Integer call() throws Exception {
        int result = 1;
        if (a<1) {
            throw new Exception("Incorrect value");
        }
        for (int i=1;i<=a;i++) {
            result *= i;
        }
        return result;
    }
}
