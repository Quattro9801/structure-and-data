package com.mycompany.concurrency.threadpool.callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = Executors.newSingleThreadExecutor();
       Future<Integer> future =  service.submit(new Logic(5));
        //Thread.sleep(5000);
        Integer res = null;
        try {
            res= future.get();
        } catch (ExecutionException e) {
            System.out.println(e.getCause());
        }
        finally {
            service.shutdown();

        }
        System.out.println(res);
    }
}
