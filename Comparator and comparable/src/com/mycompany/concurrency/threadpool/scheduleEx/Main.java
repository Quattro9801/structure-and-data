package com.mycompany.concurrency.threadpool.scheduleEx;

import com.mycompany.concurrency.threadpool.ThreadEx;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
      /*  for (int i=0;i<10;i++) {
            service.execute(new ScheduleThread());
        }*/
        // service.schedule(new ScheduleThread(),3, TimeUnit.SECONDS);//указывает через сколько будет выполнено задание
        service.scheduleWithFixedDelay(new ScheduleThread(),3,1,TimeUnit.SECONDS);//задание
        // будет выполнено через 3 сек с периодичностью в 1 секунду
        //service.shutdown();
        Thread.sleep(20000);
        service.shutdown();
    }
}
