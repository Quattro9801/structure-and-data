package com.mycompany.concurrency.threadpool.scheduleEx;

public class ScheduleThread extends Thread {
    @Override
    public void run() {
        System.out.println("Start");
        System.out.println(Thread.currentThread().getName());

        try {
            sleep(5000);
            System.out.println("Finish");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
