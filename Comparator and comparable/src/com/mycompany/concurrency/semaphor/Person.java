package com.mycompany.concurrency.semaphor;

import java.util.concurrent.Semaphore;

public class Person extends Thread {

    public String name;
    private Semaphore callBox;

    public Person(String name,Semaphore callBox) {
        this.name = name;
        this.callBox = callBox;
        this.start();
    }
    @Override
    public void run() {
        try {
            System.out.println("Ждет очереди " + name);
            callBox.acquire();
            System.out.println("Пользуется телефоном " + name);
            sleep(2000);
            System.out.println("Закончил пользоваться " + name);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        finally {
            callBox.release();
        }
    }
}
