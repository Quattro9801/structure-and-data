package com.mycompany.concurrency.semaphor;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);
        Person p1 = new Person("Ivan", semaphore);
        Person p2 = new Person("Dmitrii", semaphore);
        Person p3 = new Person("Olga", semaphore);
        Person p4 = new Person("Elena", semaphore);
        Person p5 = new Person("Evgenii", semaphore);
    }
}
