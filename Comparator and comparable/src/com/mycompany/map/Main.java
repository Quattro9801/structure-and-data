package com.mycompany.map;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student(23,"Ivan","Nikolaev");
        Student student2 = new Student(24,"Oleg","Nikolaev");
        Student student3 = new Student(23,"Olga","Nikolaev");
        Student student4 = new Student(23,"Tanya","Nikolaev");

        Student student5 = new Student(23,"Tanya","Nikolaev");
        Map<Student, Double> map = new HashMap<>();
        map.put(student1, 4.2);
        map.put(student2, 4.5);
        map.put(student3, 3.8);
        map.put(student4, 4.2);
        map.put(student5, 4.9);
        System.out.println(map);
        boolean isEqual = map.containsKey(student5);
        System.out.println(isEqual);
        System.out.println("Equals " + student4.equals(student5));
        System.out.println("HashCode st4 " + student4.hashCode());
        System.out.println("HashCode st5 " + student5.hashCode());
    }
}
