package com.mycompany.collectionsEx.arrayList;

import java.util.List;

public class Sublist {

    public static List<String> getSublist(List<String> list) {//sublist не создает новый лист, он создает представление
        List<String> newlist = list.subList(0,2);
        newlist.add("123455");
        //list.add("sdsds");//можем модифицировать только view иначе, будет main" java.util.ConcurrentModificationException

        return newlist;
    }
}
