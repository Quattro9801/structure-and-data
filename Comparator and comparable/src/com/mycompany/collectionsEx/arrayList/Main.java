package com.mycompany.collectionsEx.arrayList;

import java.util.*;

public class Main {
    //removeAll - удалит элементы из первого листа, которые есть во втором
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("d");
        list.add("d");
        list.add("d");

        List<String> list2 = new ArrayList<>();
        list2.add("c");
        list2.add("d");
        //list.removeAll(list2);
        //list.retainAll(list2);// удалит элементы, которых нет во 2 списке
        //System.out.println(list);
        //System.out.println(Sublist.getSublist(list));
        //System.out.println(list);
        //приведение к массиву
       /* String [] mass = list.toArray(new String[0]);
        for (int i=0;i < list.size();i++) {
            System.out.println(mass[i]);
        }*/


        //итератор, отличие от foreach в том, что еще можно удалять элементы
        Iterator<String> iterator = list.iterator();
     /*   while (iterator.hasNext()) {
            //System.out.println(iterator.next());
        }*/

        ListIterator<String> listIter = list.listIterator();//???
        while (listIter.hasPrevious()) {
            //listIter.previous();
            System.out.println(listIter.previous());
        }
        Vector<Integer> vector = new Vector<>();
        vector.add(1);
        vector.add(2);
        vector.add(3);
        System.out.println(vector.firstElement());

        Stack<String> stack = new Stack<>();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");
        System.out.println(stack);
        System.out.println("Element " + stack.peek());
        System.out.println(stack);

        System.out.println("---------------------------------------");
        System.out.println("Queue");
        Queue<String> queue = new LinkedList<>();
        queue.add("S");
        queue.add("B");
        queue.add("A");
        queue.add("C");
        System.out.println("Queue " + queue);
        System.out.println("Queue peek" + queue.peek());
        System.out.println("Queue after peek" + queue);
        //отличия методов remove и poll. Если удалять больше элементов чем есть в коллекции, то выбросится exception.Метод poll вернет null.
        queue.remove();
        //queue.element и queue.peek возвращает первый элемент, если элементов больше нет element вернет exception, peek вернет null
        // можно удалять из середины очереди
        queue.remove("A");
        System.out.println(queue);

        System.out.println("------------------------");
        System.out.println("PriorityQueue");
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();// приоритет у самого наименьшего элемента, но при выводе элементов порядок будет другим
        priorityQueue.add(89);
        priorityQueue.add(9);// этот элемент будет первым
        priorityQueue.add(9998);
        priorityQueue.add(23);
        priorityQueue.add(55);
        System.out.println(priorityQueue);
        System.out.println(priorityQueue.remove());
        System.out.println(priorityQueue.remove());
        System.out.println(priorityQueue.remove());
        System.out.println(priorityQueue.remove());
        System.out.println(priorityQueue.remove());
        System.out.println("-----------------");
        System.out.println("Deque");
        Deque<Integer> deque = new ArrayDeque<>();
        deque.add(1);
        deque.addFirst(0);
        deque.add(78);
        deque.add(56);
        deque.add(995);
        System.out.println(deque.getFirst());
        System.out.println(deque);




    }
}
