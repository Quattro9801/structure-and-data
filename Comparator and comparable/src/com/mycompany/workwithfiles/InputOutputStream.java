package com.mycompany.workwithfiles;

import java.io.*;

public class InputOutputStream {
    public static void main(String[] args) throws FileNotFoundException {// копирование картинки
        try (
                FileInputStream in = new FileInputStream("C:\\Users\\Quattro\\Downloads\\1234hh.jpeg");
                FileOutputStream out = new FileOutputStream("1234hh.jpeg")
                ) {
            int character;
            while ((character = in.read()) != -1)//концом файла считается -1
            {
                out.write(character);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
