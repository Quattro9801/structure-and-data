package com.mycompany.workwithfiles;

import javax.sound.sampled.AudioFileFormat;
import java.io.*;

public class BufferedReaderWriter {
    public static void main(String[] args) {

        try (BufferedReader fileReader = new BufferedReader(new FileReader("E:\\repository\\Structure and data\\resources\\example1.txt"));
             BufferedWriter fileWriter = new BufferedWriter(new FileWriter("test5"));){
         /*   int character;
            while ((character = fileReader.read()) != -1)//концом файла считается -1
            {
                System.out.print((char) character);
                fileWriter.write(character);
            }*/

            String line;
            while ((line = fileReader.readLine()) != null)//чтение не посимвольно а построчно
            {
                System.out.print(line);
                fileWriter.write(line);
                fileWriter.write("\n");
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
