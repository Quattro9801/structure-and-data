package com.mycompany.workwithfiles;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ObjectOutputStream {
    public static void main(String[] args) throws FileNotFoundException {
        Home home = new Home(4,"Red",600);
        Dwarf dwarf = new Dwarf("Vova", 26,"Test",home, 100);
        try (
                java.io.ObjectOutputStream writeToFile = new java.io.ObjectOutputStream(Files.newOutputStream(Paths.get("serialization.bin")));
                java.io.ObjectInputStream readFromFile = new java.io.ObjectInputStream(Files.newInputStream(Paths.get("serialization.bin")))
                )
        {
            writeToFile.writeObject(dwarf);
            System.out.println("Done");
            System.out.println((Dwarf) readFromFile.readObject());

        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
      }
    }
}
