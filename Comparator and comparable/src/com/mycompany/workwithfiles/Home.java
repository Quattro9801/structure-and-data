package com.mycompany.workwithfiles;

import java.io.Serializable;

public class Home implements Serializable {
    private int doors;
    private String color;

    private int cost;

    public Home(int doors, String color, int cost) {
        this.doors = doors;
        this.color = color;
        this.cost = cost;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Home{" +
                "doors=" + doors +
                ", color='" + color + '\'' +
                ", cost=" + cost +
                '}';
    }
}
