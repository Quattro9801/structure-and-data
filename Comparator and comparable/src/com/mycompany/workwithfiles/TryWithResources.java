package com.mycompany.workwithfiles;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TryWithResources {
    public static void main(String[] args) throws IOException {

        try (
            FileReader fileReader = new FileReader("E:\\repository\\Structure and data\\resources\\example1.txt");) {
            int character;
            while ((character = fileReader.read()) != -1)//концом файла считается -1
            {
                System.out.print((char) character);
            }
        }
    }
}
