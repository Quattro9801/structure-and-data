package com.mycompany.workwithfiles;

import java.io.Serializable;

public class Dwarf implements Serializable {
    private static final long serialVersionUID = 2;
    private String name;
    private int age;
    private String description;

    private transient Home home;

    private int damage;

    public Dwarf(String name, int age, String description, Home home, int damage) {
        this.name = name;
        this.age = age;
        this.description = description;
        this.home = home;
        this.damage = damage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        return "Dwarf{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", description='" + description + '\'' +
                ", home=" + home +
                ", damage=" + damage +
                '}';
    }
}
