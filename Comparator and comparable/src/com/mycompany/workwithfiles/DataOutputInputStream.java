package com.mycompany.workwithfiles;

import java.io.*;

public class DataOutputInputStream {
    public static void main(String[] args) {
        try (DataOutputStream fileWriter = new DataOutputStream(new FileOutputStream("first.bin"));
             DataInputStream fileReader = new DataInputStream(new FileInputStream("test5"));){
         /*   int character;
            while ((character = fileReader.read()) != -1)//концом файла считается -1
            {
                System.out.print((char) character);
                fileWriter.write(character);
            }*/
            fileWriter.writeInt(1);
            fileWriter.writeBoolean(true);
            fileWriter.writeLong(12345L);
            System.out.println(fileReader.readBoolean());

            } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
