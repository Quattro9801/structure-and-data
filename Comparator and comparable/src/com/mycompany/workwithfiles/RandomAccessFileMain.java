package com.mycompany.workwithfiles;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessFileMain {
    public static void main(String[] args) throws IOException {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile("randomAccess.txt","rw");)//2 параметр режим работы с файлом
        {
        /*    int symbol = randomAccessFile.read();//первый символ файла
            System.out.println((char) symbol);
            symbol = randomAccessFile.read();
            System.out.println((char) symbol);*///следующий символ
             String line = randomAccessFile.readLine();//построчное чтение
            System.out.println(line);
             line = randomAccessFile.readLine();
            System.out.println(line);
            randomAccessFile.seek(45);// с какой позиции читать
            line = randomAccessFile.readLine();
            System.out.println(line);
            System.out.println(randomAccessFile.getFilePointer());//узгать позицию
            randomAccessFile.seek(randomAccessFile.length()-1);// где сейчас находимся
            randomAccessFile.writeBytes("\n\t\t\t\t George Gordon Byron");
        }
    }
}
