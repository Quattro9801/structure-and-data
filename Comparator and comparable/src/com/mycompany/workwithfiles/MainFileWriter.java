package com.mycompany.workwithfiles;

import java.io.FileWriter;
import java.io.IOException;

public class MainFileWriter {
    public static void main(String[] args) throws IOException {
        String text = "1234\n" + "123456\n" + "987\n" + "678\n";
        FileWriter fileWriter = null;
        try {
             fileWriter = new FileWriter("E:\\repository\\Structure and data\\resources\\example1.txt",true);//2 параметр указывает, на то что при добавлении новой строки нужно дописывать в файл или перезаписывать
            for (int i=0;i<text.length();i++) {
                fileWriter.write(text.charAt(i));
            }
            fileWriter.write("+++++++++");
            System.out.println("End");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            fileWriter.close();
        }
    }}
