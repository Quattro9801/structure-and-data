package com.mycompany.workwithfiles.fileClass;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

public class FileMain {
    public static void main(String[] args) throws IOException {
        File file = new File("randomAccess.txt");
        File fileFolder = new File("E:\\repository\\Structure and data");

        File file2 = new File("11111.txt");
        File fileFolder1 = new File("E:\\repository\\Structure and data\\RandomAccess");

        System.out.println("absolutePath file " + file.getAbsolutePath());
        System.out.println("absolutePath folder " + fileFolder.getAbsolutePath());

        System.out.println("absolutePath file " + file.isAbsolute());
        System.out.println("absolutePath folder " + fileFolder.isAbsolute());

        System.out.println("Check existing file " + file.exists());
        System.out.println("Check existing folder " + fileFolder.exists());

        System.out.println("Check existing file2 " + file2.exists());
        System.out.println("Check existing folder1 " + fileFolder1.exists());

        file2.createNewFile();// создаст пустой файл
        fileFolder1.mkdir();//создаст директорию

        System.out.println("Length " + file2.length());
        System.out.println("Delete " + file2.delete());

        File [] files = fileFolder.listFiles();// выоводит путь с файлами в папках и подпапках
        System.out.println(Arrays.toString(files));





    }

}
